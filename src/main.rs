use double_image::create_double_image;

fn main() {
    let image0 = raster::open("./test-images/image0.jpg").expect("Unable to open image0");
    let image1 = raster::open("./test-images/image1.jpg").expect("Unable to open image1");
    let width = image0.width;
    let height = image0.height;
    let double_image = create_double_image(width, height, &image0, &image1).expect("Unable to create double_image");
    raster::save(&double_image, "./test-images/double_image.jpg").expect("Unable to save double_image");
}
