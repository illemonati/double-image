use raster::{Image, editor, ResizeMode, error::RasterResult, Color};


const IMAGE0_WEIGHT: f32 = 0.9;
const IMAGE1_WEIGHT: f32 = 0.1;

pub fn create_double_image(width: i32, height: i32, image0: &Image, image1: &Image) -> RasterResult<Image> {
    let mut output = Image::blank(width, height);
    let mut image0 = image0.clone();
    let mut image1 = image1.clone();
    editor::resize(&mut image0, width, height, ResizeMode::Fill)?;
    editor::resize(&mut image1, width, height, ResizeMode::Fill)?;
    for row in 0..height {
        for col in 0..width {

            if row % 2 == 0 || col % 2 == 0 {
                output.set_pixel(col, row, Color::rgb(0,0,0))?;
                continue;
            }


            let image0_pixel = image0.get_pixel(col, row)?;
            let image1_pixel = image1.get_pixel(col, row)?;

            // if row % 4 == 0 && col % 4 == 0 {
            //     output.set_pixel(col, row, Color::rgb(255, 255, 255))?;
            //     continue;
            // }

            // if row % 4 == 2 && col % 4 == 2 {
            //     output.set_pixel(col, row, Color::rgb(0,0,0))?;
            //     continue;
            // }


            // if row % 4 == 3 && col % 4 == 3 {
            //     output.set_pixel(col, row, image0_pixel)?;
            //     continue;
            // }
            
            // output.set_pixel(col, row, image1_pixel)?;
            
            // output.set_pixel(col, row, image0_pixel)?;

            let average_r: u8 = (image0_pixel.r as f32* IMAGE0_WEIGHT + image1_pixel.r as f32 * IMAGE1_WEIGHT) as u8;
            let average_g: u8 = (image0_pixel.g as f32* IMAGE0_WEIGHT + image1_pixel.g as f32 * IMAGE1_WEIGHT) as u8;
            let average_b: u8 = (image0_pixel.b as f32* IMAGE0_WEIGHT + image1_pixel.b as f32 * IMAGE1_WEIGHT) as u8;
            let average_pixel = Color::rgba(average_r, average_g, average_b, 255);
            output.set_pixel(col, row, average_pixel)?;
        }
    }
    Ok(output)
}